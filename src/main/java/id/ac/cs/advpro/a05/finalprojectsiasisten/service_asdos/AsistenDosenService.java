package id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;

import java.util.List;

public interface AsistenDosenService {
    List<AsistenDosen> getAsistenDosenByIdMataKuliah(String id);
}
