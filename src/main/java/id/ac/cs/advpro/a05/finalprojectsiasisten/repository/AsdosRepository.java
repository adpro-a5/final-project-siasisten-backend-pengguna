package id.ac.cs.advpro.a05.finalprojectsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AsdosRepository extends JpaRepository<AsistenDosen, String>{
    List<AsistenDosen> findAllByMataKuliahIdMataKuliah(String id);
    AsistenDosen findAsistenDosenByMahasiswaAndMataKuliah(Mahasiswa mahasiswa, MataKuliah mataKuliah);
    List<AsistenDosen> findAllByMahasiswaAkunEmail(String email);
}
