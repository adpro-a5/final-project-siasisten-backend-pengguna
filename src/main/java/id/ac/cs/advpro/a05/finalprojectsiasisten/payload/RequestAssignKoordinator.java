package id.ac.cs.advpro.a05.finalprojectsiasisten.payload;

import lombok.Data;

@Data
public class RequestAssignKoordinator {
    private String npmMahasiswa;
    private String idMataKuliah;
}
