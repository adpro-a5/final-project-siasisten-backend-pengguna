package id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "DosenMK")
@NoArgsConstructor
public class DosenMK {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "nidn_dosen")
    private Dosen dosen;

    @ManyToOne
    @JoinColumn(name = "id_matkul")
    private MataKuliah mataKuliah;

    public DosenMK(Dosen dosen, MataKuliah mataKuliah) {
        this.dosen = dosen;
        this.mataKuliah = mataKuliah;
    }
}
