package id.ac.cs.advpro.a05.finalprojectsiasisten.payload;

import lombok.Data;

import java.util.Map;

@Data
public class RequestCreateDosen {
    private Map<String, String> akun;
    private Map<String, String> dosen;
}
