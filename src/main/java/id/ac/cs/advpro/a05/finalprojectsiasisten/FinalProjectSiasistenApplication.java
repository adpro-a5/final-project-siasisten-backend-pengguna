package id.ac.cs.advpro.a05.finalprojectsiasisten;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalProjectSiasistenApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinalProjectSiasistenApplication.class, args);
    }

}
