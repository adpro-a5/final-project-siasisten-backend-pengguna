package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestAssignKoordinator;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos.KoordinatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/koordinator")
public class KoordinatorApiController {

    @Autowired
    private KoordinatorService koordinatorService;

    @PostMapping()
    public ResponseEntity<Object> assignKoordinator(@RequestBody RequestAssignKoordinator assignKoordinator) {
        koordinatorService.assignKoor(assignKoordinator.getNpmMahasiswa(), assignKoordinator.getIdMataKuliah());
        return ResponseEntity.ok("Asisten dosen berhasil menjadi koordinator.");
    }

}
