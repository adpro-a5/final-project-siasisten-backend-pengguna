package id.ac.cs.advpro.a05.finalprojectsiasisten.payload;

import lombok.Data;

import java.util.List;

@Data
public class RequestCreateMataKuliah {
    private String idMataKuliah;
    private String namaMataKuliah;
    private String jumlahLowongan;
    private List<String> dosen;
}
