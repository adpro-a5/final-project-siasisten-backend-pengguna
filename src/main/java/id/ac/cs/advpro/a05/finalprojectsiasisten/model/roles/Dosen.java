package id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Dosen")
@NoArgsConstructor
public class Dosen  {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "email")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Akun akun;

    @Id
    @Column(nullable = false)
    private String nidnDosen;

    @Column(nullable = false)
    private String namaDosen;

    @Column
    private Boolean isApprove = false;

    public Dosen(Akun akun, String nidnDosen, String namaDosen) {
        this.akun = akun;
        this.nidnDosen = nidnDosen;
        this.namaDosen = namaDosen;
    }
}
