package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreateMahasiwa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/mahasiswa")
public class MahasiswaApiController {

    @Autowired
    private MahasiswaService mahasiswaService;

    @GetMapping("")
    public ResponseEntity<List<Mahasiswa>> getAllMahasiswa(){
        return ResponseEntity.ok(mahasiswaService.fetchAllMahasiswa());
    }

    @GetMapping(path = "/{email}", produces = {"application/json"})
    public ResponseEntity<Mahasiswa> getMahasiswa(@PathVariable(value = "email") String email) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByEmail(email);
        if (mahasiswa == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(mahasiswa);
    }

    @PostMapping("")
    public ResponseEntity<Mahasiswa> createMahasiswa(@RequestBody RequestCreateMahasiwa createMahasiwa) {
        return ResponseEntity.ok(mahasiswaService.addMahasiswa(createMahasiwa.getAkun(), createMahasiwa.getMahasiswa()));
    }

    @PutMapping(path = "/{npm}", produces = {"application/json"})
    public ResponseEntity<Mahasiswa> updateMahasiswa(@PathVariable(value = "npm")  String npm, @RequestBody Map<String, String> updated){
        return ResponseEntity.ok(mahasiswaService.updateMahasiswa(npm, updated));
    }

}
