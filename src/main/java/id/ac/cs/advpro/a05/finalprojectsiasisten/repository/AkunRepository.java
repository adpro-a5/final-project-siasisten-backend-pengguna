package id.ac.cs.advpro.a05.finalprojectsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AkunRepository extends JpaRepository<Akun, String> {
    Optional<Akun> findAkunByEmail(String email);

    void deleteAkunByEmail(String email);
}