package id.ac.cs.advpro.a05.finalprojectsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Koordinator;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KoordinatorRepository extends JpaRepository<Koordinator, String> {

    List<Koordinator> findAllByMataKuliah(MataKuliah mataKuliah);

    Koordinator findKoordinatorByMahasiswaAndMataKuliah(Mahasiswa mahasiswa, MataKuliah mataKuliah);

    List<Koordinator> findAll();
}