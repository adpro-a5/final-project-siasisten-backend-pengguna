package id.ac.cs.advpro.a05.finalprojectsiasisten.payload;

import lombok.Data;

import java.util.Map;

@Data
public class RequestCreateMahasiwa {
    private Map<String, String> akun;
    private Map<String, String> mahasiswa;
}
