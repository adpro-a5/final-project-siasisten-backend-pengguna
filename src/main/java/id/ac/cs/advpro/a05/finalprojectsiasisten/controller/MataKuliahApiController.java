package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreateMataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_matkul.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/mata-kuliah")
public class MataKuliahApiController {

    @Autowired
    private MataKuliahService mataKuliahService;

    @GetMapping("")
    public List<MataKuliah> getAllMataKuliah(){
        return mataKuliahService.fetchAllMataKuliah();
    }

    @PostMapping("")
    public ResponseEntity<MataKuliah> createMataKuliah(@RequestBody RequestCreateMataKuliah createMataKuliah) {
        return ResponseEntity.ok(mataKuliahService.addMataKuliah(createMataKuliah));
    }
}
