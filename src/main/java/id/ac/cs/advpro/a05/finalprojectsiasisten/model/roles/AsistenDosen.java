package id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "AsistenDosen")
@NoArgsConstructor
public class AsistenDosen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "npmMahasiswa")
    private Mahasiswa mahasiswa;

    @ManyToOne
    @JoinColumn(name = "id_matkul")
    private MataKuliah mataKuliah;

    @Column
    private Boolean isKoordinator = false;

    public AsistenDosen(Mahasiswa mahasiswa, MataKuliah mataKuliah) {
        this.mahasiswa = mahasiswa;
        this.mataKuliah = mataKuliah;
    }
}
