package id.ac.cs.advpro.a05.finalprojectsiasisten.payload;

import lombok.Data;

@Data
public class RequestUpdateMahasiswa {
    private String nomorTelepon;
    private String waktuKosong;
    private String namaBank;
    private String nomorRekening;
}
