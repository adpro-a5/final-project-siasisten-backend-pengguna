package id.ac.cs.advpro.a05.finalprojectsiasisten.payload;

import lombok.Data;

@Data
public class RequestCreatePendaftaran {
    private String idMataKuliah;
    private String npmMahasiswa;
    private int sks;
    private double ipk;
}
