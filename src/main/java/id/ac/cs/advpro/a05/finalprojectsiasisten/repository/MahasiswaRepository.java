package id.ac.cs.advpro.a05.finalprojectsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MahasiswaRepository extends JpaRepository<Mahasiswa, String> {
    Mahasiswa findMahasiswaByNpmMahasiswa(String npm);

    Mahasiswa findByAkunEmail(String email);

    void deleteMahasiswaByNpmMahasiswa(String npm);
}
