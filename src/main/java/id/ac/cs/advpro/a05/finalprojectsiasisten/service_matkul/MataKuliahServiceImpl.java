package id.ac.cs.advpro.a05.finalprojectsiasisten.service_matkul;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.DosenMK;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreateMataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AsdosRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.DosenMKRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.DosenRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MataKuliahServiceImpl implements MataKuliahService{

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private DosenMKRepository dosenMKRepository;

    @Autowired
    private DosenRepository dosenRepository;

    @Autowired
    private AsdosRepository asdosRepository;

    @Override
    public List<MataKuliah> fetchAllMataKuliah() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return mataKuliahRepository.findAllByOrderByNamaMataKuliah();
    }

    @Override
    public MataKuliah addMataKuliah(RequestCreateMataKuliah createMataKuliah) {
        String idMataKuliah = String.valueOf(createMataKuliah.getIdMataKuliah());
        String namaMataKuliah = String.valueOf(createMataKuliah.getNamaMataKuliah());
        String jumlahLowongan = String.valueOf(createMataKuliah.getJumlahLowongan());
        int intJumlahLowongan;

        if (idMataKuliah.equals("")) return new MataKuliah();
        if (jumlahLowongan.equals("")) intJumlahLowongan = 0;
        else intJumlahLowongan = Integer.parseInt(jumlahLowongan);

        MataKuliah mataKuliah = new MataKuliah(idMataKuliah, namaMataKuliah, intJumlahLowongan);
        mataKuliah.setRegisteredAt(new Date());
        mataKuliahRepository.save(mataKuliah);

        List<String> listDosen = createMataKuliah.getDosen();
        addDosenMK(mataKuliah, listDosen);

        return mataKuliah;
    }

    @Override
    public List<DosenMK> getMataKuliahByEmailDosen(String email) {
        return dosenMKRepository.findAllByDosenAkunEmail(email);
    }

    @Override
    public List<AsistenDosen> getMataKuliahByEmailAsdos(String email) {
        return asdosRepository.findAllByMahasiswaAkunEmail(email);
    }

    private void addDosenMK(MataKuliah mataKuliah, List<String> listDosen) {
        for (String i: listDosen) {
            Dosen dosen = dosenRepository.findDosenByNidnDosen(i);
            dosenMKRepository.save(new DosenMK(dosen, mataKuliah));
        }
    }
}
