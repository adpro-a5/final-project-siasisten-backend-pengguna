package id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Entity
@Table(name = "Akun")
@NoArgsConstructor
public class Akun implements Serializable, UserDetails {
    @Id
    @Column(name = "email")
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String nomorTelepon;

    @Column
    @Enumerated(EnumType.STRING)
    private Roles role;

    @Column
    private SimpleGrantedAuthority authority;

    public Akun(String email, String password, String nomorTelepon, Roles role) {
        this.email = email;
        this.password = password;
        this.nomorTelepon = nomorTelepon;
        this.role = role;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(authority);

        return authorities;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
