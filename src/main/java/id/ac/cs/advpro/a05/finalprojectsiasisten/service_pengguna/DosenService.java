package id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;

import java.util.List;
import java.util.Map;

public interface DosenService {
    List<Dosen> fetchAllDosen();
    Dosen addDosen(Map<String, String> akunMap, Map<String, String> dosenMap);
    Dosen getDosenByEmail(String email);
    Dosen updateDosen(String nidnDosen, Boolean action);

}
