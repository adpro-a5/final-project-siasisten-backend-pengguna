package id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;

import java.util.List;
import java.util.Map;

public interface MahasiswaService {
    List<Mahasiswa> fetchAllMahasiswa();

    Mahasiswa getMahasiswaByEmail(String email);

    Mahasiswa addMahasiswa(Map<String, String> akunMap, Map<String, String> mahasiswaMap);

    Mahasiswa updateMahasiswa(String npm, Map<String, String> updated);
}
