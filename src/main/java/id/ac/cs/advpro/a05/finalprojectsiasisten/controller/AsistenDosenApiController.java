package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos.AsistenDosenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/asisten-dosen")
public class AsistenDosenApiController {

    @Autowired
    private AsistenDosenService asistenDosenService;

    @GetMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<List<AsistenDosen>> getAsistenDosenByIdMataKuliah(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(asistenDosenService.getAsistenDosenByIdMataKuliah(id));
    }

}
