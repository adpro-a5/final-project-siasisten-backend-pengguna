package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@Controller
@RequestMapping(path = "/auth/logout")
public class LogoutApiController {

    @PostMapping("")
    public ResponseEntity<Object> logoutUser() {
        SecurityContextHolder.clearContext();
        return ResponseEntity.ok(SecurityContextHolder.getContext().getAuthentication());
    }

}

