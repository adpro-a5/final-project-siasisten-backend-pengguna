package id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos;

import id.ac.cs.advpro.a05.finalprojectsiasisten.core.Authority;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.*;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AsdosRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.KoordinatorRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MahasiswaRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KoordinatorServiceImpl implements KoordinatorService {

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private KoordinatorRepository koordinatorRepository;

    @Autowired
    private AsdosRepository asdosRepository;

    @Override
    public void assignKoor(String mahasiswa, String mataKuliah) {
        Mahasiswa mahasiswa1 = mahasiswaRepository.findMahasiswaByNpmMahasiswa(mahasiswa);
        MataKuliah mataKuliah1 = mataKuliahRepository.findMataKuliahByIdMataKuliah(mataKuliah);
        AsistenDosen asistenDosen = asdosRepository.findAsistenDosenByMahasiswaAndMataKuliah(mahasiswa1, mataKuliah1);
        Koordinator koordinator = new Koordinator();

        if (koordinatorRepository.findKoordinatorByMahasiswaAndMataKuliah(mahasiswa1, mataKuliah1) == null) {
            Akun akun = mahasiswa1.getAkun();
            akun.setRole(Roles.KOORDINATOR);
            akun.setAuthority(Authority.getInstance(Roles.KOORDINATOR.name()).getAuthority());
            koordinator.setMahasiswa(mahasiswa1);
            koordinator.setMataKuliah(mataKuliah1);
            koordinatorRepository.save(koordinator);
            asistenDosen.setIsKoordinator(true);
            asdosRepository.save(asistenDosen);
        }

    }

}
