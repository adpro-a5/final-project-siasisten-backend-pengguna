package id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos;

import id.ac.cs.advpro.a05.finalprojectsiasisten.core.Authority;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.pendaftaran.Pendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.pendaftaran.StatusPendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestActionPendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreatePendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AsdosRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MahasiswaRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MataKuliahRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.PendaftaranRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PendaftaranServiceImpl implements PendaftaranService {

    @Autowired
    private PendaftaranRepository pendaftaranRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private AsdosRepository asdosRepository;

    @Override
    public Pendaftaran getPendaftaran(String idMataKuliah, String npmMahasiswa) {
        return pendaftaranRepository.findPendaftaranByMahasiswaNpmMahasiswaAndMataKuliahIdMataKuliah(npmMahasiswa, idMataKuliah);
    }

    @Override
    public Pendaftaran addPendaftaran(RequestCreatePendaftaran createPendaftaran) {
        MataKuliah mataKuliah = mataKuliahRepository.findMataKuliahByIdMataKuliah(createPendaftaran.getIdMataKuliah());
        Mahasiswa mahasiswa = mahasiswaRepository.findMahasiswaByNpmMahasiswa(createPendaftaran.getNpmMahasiswa());
        Pendaftaran pendaftaran = new Pendaftaran(mahasiswa, mataKuliah);
        pendaftaran.setSks(createPendaftaran.getSks());
        pendaftaran.setIpk(createPendaftaran.getIpk());
        mataKuliah.setJumlahPelamar(mataKuliah.getJumlahPelamar() + 1);
        mataKuliahRepository.save(mataKuliah);
        pendaftaranRepository.save(pendaftaran);
        return pendaftaran;
    }

    @Override
    public List<Pendaftaran> getPendaftaranByMataKuliah(String id) {
        MataKuliah mataKuliah = mataKuliahRepository.findMataKuliahByIdMataKuliah(id);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return pendaftaranRepository.findAllByMataKuliah(mataKuliah);
    }

    @Override
    public Pendaftaran actionAsdos(String id, RequestActionPendaftaran actionPendaftaran) {
        MataKuliah mataKuliah = mataKuliahRepository.findMataKuliahByIdMataKuliah(id);
        Mahasiswa mahasiswa = mahasiswaRepository.findMahasiswaByNpmMahasiswa(actionPendaftaran.getNpmMahasiswa());
        String action = actionPendaftaran.getAction();
        Pendaftaran pendaftaran = pendaftaranRepository.findPendaftaranByMahasiswaAndMataKuliah(mahasiswa, mataKuliah);
        pendaftaran.setStatusPendaftaran(StatusPendaftaran.valueOf(action));
        pendaftaranRepository.save(pendaftaran);

        if (action.equals("DITERIMA")) {
            Akun akun = mahasiswa.getAkun();
            akun.setRole(Roles.ASISTENDOSEN);
            akun.setAuthority(Authority.getInstance(Roles.ASISTENDOSEN.name()).getAuthority());
            mataKuliah.setJumlahAsisten(mataKuliah.getJumlahAsisten()+1);
            mataKuliahRepository.save(mataKuliah);
            asdosRepository.save(new AsistenDosen(mahasiswa, mataKuliah));
        }
        return pendaftaran;
    }

}
