package id.ac.cs.advpro.a05.finalprojectsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DosenRepository extends JpaRepository<Dosen, String> {
    Dosen findDosenByNidnDosen(String nidn);

    Dosen findByAkunEmail(String email);

    void deleteDosenByNidnDosen(String nidn);
}
