package id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.pendaftaran.Pendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestActionPendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreatePendaftaran;

import java.util.List;

public interface PendaftaranService {
    List<Pendaftaran> getPendaftaranByMataKuliah(String id);
    Pendaftaran getPendaftaran(String idMataKuliah, String npmMahasiswa);
    Pendaftaran addPendaftaran(RequestCreatePendaftaran mapPendaftaran);
    Pendaftaran actionAsdos(String id, RequestActionPendaftaran actionPendaftaran);

}
