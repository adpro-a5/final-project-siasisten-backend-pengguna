package id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Admin")
@NoArgsConstructor
public class Admin {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "email")
    private Akun akun;

    @Id
    @Column(nullable = false)
    private String idAdmin;

    public Admin(Akun akun, String idAdmin) {
        this.akun = akun;
        this.idAdmin = idAdmin;
    }
}
