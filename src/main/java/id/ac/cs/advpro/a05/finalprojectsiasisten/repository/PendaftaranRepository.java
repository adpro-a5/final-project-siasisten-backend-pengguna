package id.ac.cs.advpro.a05.finalprojectsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.pendaftaran.Pendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PendaftaranRepository extends JpaRepository<Pendaftaran, String> {
    Pendaftaran findPendaftaranByMahasiswaAndMataKuliah(Mahasiswa mahasiswa, MataKuliah mataKuliah);
    Pendaftaran findPendaftaranByMahasiswaNpmMahasiswaAndMataKuliahIdMataKuliah(String npmMahasiswa, String idMataKuliah);
    List<Pendaftaran> findAllByMataKuliah(MataKuliah mataKuliah);
}
