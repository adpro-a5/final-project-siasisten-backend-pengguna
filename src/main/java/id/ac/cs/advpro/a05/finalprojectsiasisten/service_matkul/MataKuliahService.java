package id.ac.cs.advpro.a05.finalprojectsiasisten.service_matkul;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.DosenMK;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreateMataKuliah;

import java.util.List;

public interface MataKuliahService {
    List<MataKuliah> fetchAllMataKuliah();
    MataKuliah addMataKuliah(RequestCreateMataKuliah mapBody);
    List<DosenMK> getMataKuliahByEmailDosen(String email);
    List<AsistenDosen> getMataKuliahByEmailAsdos(String email);
}
