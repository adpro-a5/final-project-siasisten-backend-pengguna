package id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MataKuliah")
@NoArgsConstructor
public class MataKuliah implements Serializable {
    @Id
    @Column(name = "id_matkul")
    private String idMataKuliah;

    @Column(name = "nama_mata_kuliah", unique = true, nullable = false)
    private String namaMataKuliah;

    @Column
    private int jumlahLowongan;

    @Column
    private int jumlahPelamar;

    @Column
    private int jumlahAsisten;

    @CreatedDate
    @Column
    private Date registeredAt;

    public MataKuliah(String idMataKuliah, String namaMataKuliah, int jumlahLowongan) {
        this.idMataKuliah = idMataKuliah;
        this.namaMataKuliah = namaMataKuliah;
        this.jumlahLowongan = jumlahLowongan;
    }
}
