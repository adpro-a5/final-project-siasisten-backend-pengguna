package id.ac.cs.advpro.a05.finalprojectsiasisten.core;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class Authority {
    private static Authority instance;
    private static SimpleGrantedAuthority simpleGrantedAuthority;
    private String name;

    private Authority(String name) {
        this.name = name;
    }

    public static Authority getInstance(String name) {
        if (instance == null) {
            instance = new Authority(name);
            simpleGrantedAuthority = new SimpleGrantedAuthority(name);
        }
        return instance;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SimpleGrantedAuthority getAuthority() {
        return simpleGrantedAuthority;
    }
}
