package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestLogin;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna.AkunDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@Controller
@RequestMapping(path = "/auth/login")
public class LoginApiController {

    @Autowired
    private AkunDetailService akunDetailService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping(produces = {"application/json"})
    public ResponseEntity<Authentication> userLogin(@RequestBody RequestLogin request) {
        Optional<Akun> akun = akunDetailService.getAkunByEmail(request.getEmail());
        if (akun.isPresent() && passwordEncoder.matches(request.getPassword(), akun.get().getPassword())) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(akun.get().getAuthority());
            Authentication auth = new UsernamePasswordAuthenticationToken(akun.get(), akun.get().getEmail(), authorities);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
        return ResponseEntity.ok(SecurityContextHolder.getContext().getAuthentication());
    }
}

