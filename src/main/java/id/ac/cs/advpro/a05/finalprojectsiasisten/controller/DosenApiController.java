package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreateDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestUpdateDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna.DosenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/dosen")
public class DosenApiController {

    @Autowired
    private DosenService dosenService;

    @GetMapping(produces = {"application/json"})
    public ResponseEntity<List<Dosen>> getAllDosen(){
        return ResponseEntity.ok(dosenService.fetchAllDosen());
    }

    @GetMapping(path = "/{email}", produces = {"application/json"})
    public ResponseEntity<Dosen> getDosen(@PathVariable(value = "email") String email) {
        Dosen dosen = dosenService.getDosenByEmail(email);
        if (dosen == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(dosen);
    }

    @PostMapping(produces = {"application/json"})
    public ResponseEntity<Dosen> createDosen(@RequestBody RequestCreateDosen akunDosen) {
        return ResponseEntity.ok(dosenService.addDosen(akunDosen.getAkun(), akunDosen.getDosen()));
    }

    @PutMapping(path = "/{nidn}", produces = {"application/json"})
    public ResponseEntity<Dosen> updateDosen(@PathVariable(value = "nidn") String nidn, @RequestBody RequestUpdateDosen updateDosen){
        return ResponseEntity.ok(dosenService.updateDosen(nidn, updateDosen.getAction()));
    }

}
