package id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles;

public enum Roles {
    ADMIN, DOSEN, KOORDINATOR, ASISTENDOSEN, MAHASISWA
}
