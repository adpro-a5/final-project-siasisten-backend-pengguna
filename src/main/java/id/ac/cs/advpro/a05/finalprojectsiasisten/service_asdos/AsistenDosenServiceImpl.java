package id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AsdosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AsistenDosenServiceImpl implements AsistenDosenService {

    @Autowired
    private AsdosRepository asdosRepository;

    @Override
    public List<AsistenDosen> getAsistenDosenByIdMataKuliah(String id) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return asdosRepository.findAllByMataKuliahIdMataKuliah(id);
    }
}
