package id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Koordinator")
@NoArgsConstructor
public class Koordinator {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "id_matkul")
    private MataKuliah mataKuliah;

    @ManyToOne
    @JoinColumn(name = "npmMahasiswa")
    private Mahasiswa mahasiswa;
}
