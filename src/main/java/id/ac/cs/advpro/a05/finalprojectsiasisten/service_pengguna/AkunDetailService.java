package id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface AkunDetailService extends UserDetailsService {
    Optional<Akun> getAkunByEmail(String email);
}
