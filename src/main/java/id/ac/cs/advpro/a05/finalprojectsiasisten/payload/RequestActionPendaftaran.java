package id.ac.cs.advpro.a05.finalprojectsiasisten.payload;

import lombok.Data;

@Data
public class RequestActionPendaftaran {
    private String npmMahasiswa;
    private String action;
}
