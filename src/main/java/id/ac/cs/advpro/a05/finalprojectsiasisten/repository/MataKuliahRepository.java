package id.ac.cs.advpro.a05.finalprojectsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MataKuliahRepository extends JpaRepository<MataKuliah, String> {
    MataKuliah findMataKuliahByIdMataKuliah(String id);

    List<MataKuliah> findAllByOrderByNamaMataKuliah();
}