package id.ac.cs.advpro.a05.finalprojectsiasisten.model.pendaftaran;

public enum StatusPendaftaran {
    MENDAFTAR, REKOMENDASI, DITERIMA, DITOLAK
}
