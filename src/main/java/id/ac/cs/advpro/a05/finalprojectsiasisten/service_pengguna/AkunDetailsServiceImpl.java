package id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AkunRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AkunDetailsServiceImpl implements AkunDetailService {
    @Autowired
    private AkunRepository akunRepository;

    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return akunRepository.findAkunByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User not present"));
    }

    @Override
    public Optional<Akun> getAkunByEmail(String email) {
        return  akunRepository.findAkunByEmail(email);
    }

}
