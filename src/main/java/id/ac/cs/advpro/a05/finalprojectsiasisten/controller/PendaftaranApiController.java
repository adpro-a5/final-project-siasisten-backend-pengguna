package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.pendaftaran.Pendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestActionPendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreatePendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos.PendaftaranService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/pendaftaran")
public class PendaftaranApiController {

    @Autowired
    private PendaftaranService pendaftaranService;

    @PostMapping(produces = {"application/json"})
    public ResponseEntity<Pendaftaran> createPendaftaran(@RequestBody RequestCreatePendaftaran createPendaftaran) {
        Pendaftaran pendaftaran = pendaftaranService.getPendaftaran(
                createPendaftaran.getIdMataKuliah(), createPendaftaran.getNpmMahasiswa());
        if (pendaftaran != null) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
        return ResponseEntity.ok(pendaftaranService.addPendaftaran(createPendaftaran));
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<List<Pendaftaran>> getPendaftaranById(@PathVariable(value = "id") String id) {
        List<Pendaftaran> pendaftaran = pendaftaranService.getPendaftaranByMataKuliah(id);
        return ResponseEntity.ok(pendaftaran);
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity<Pendaftaran> actionAsdos(@PathVariable(value = "id") String id, @RequestBody RequestActionPendaftaran actionPendaftaran) {
        return ResponseEntity.ok(pendaftaranService.actionAsdos(id, actionPendaftaran));
    }

}
