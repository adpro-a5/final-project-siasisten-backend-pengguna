package id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna;

import id.ac.cs.advpro.a05.finalprojectsiasisten.core.Authority;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AkunRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.DosenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class DosenServiceImpl implements DosenService {

    @Autowired
    private DosenRepository dosenRepository;

    @Autowired
    private AkunRepository akunRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<Dosen> fetchAllDosen()  {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return dosenRepository.findAll();
    }

    @Override
    public Dosen addDosen(Map<String, String> akunMap, Map<String, String> dosenMap) {
        Akun akun = new Akun(akunMap.get("email"), akunMap.get("password"), akunMap.get("nomorTelepon"), Roles.DOSEN);
        akun.setPassword(passwordEncoder.encode(akun.getPassword()));
        akun.setAuthority(Authority.getInstance(akun.getRole().name()).getAuthority());
        akunRepository.save(akun);
        return dosenRepository.save(new Dosen(akun, dosenMap.get("nidnDosen"), dosenMap.get("namaDosen")));
    }

    @Override
    public Dosen getDosenByEmail(String email) {
        return dosenRepository.findByAkunEmail(email);
    }

    @Override
    public Dosen updateDosen(String nidnDosen, Boolean action) {
        Dosen dosen = dosenRepository.findDosenByNidnDosen(nidnDosen);
        dosen.setIsApprove(action);
        dosenRepository.save(dosen);
        return dosen;
    }

}
