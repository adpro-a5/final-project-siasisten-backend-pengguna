package id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Mahasiswa")
@NoArgsConstructor
public class Mahasiswa {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "email")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Akun akun;

    @Id
    @Column(nullable = false)
    private String npmMahasiswa;

    @Column(nullable = false)
    private String namaMahasiswa;

    @Column
    private String waktuKosong;

    @Column
    private String namaBank;

    @Column
    private String nomorRekening;

    public Mahasiswa(Akun akun, String npmMahasiswa, String namaMahasiswa) {
        this.akun = akun;
        this.npmMahasiswa = npmMahasiswa;
        this.namaMahasiswa = namaMahasiswa;
    }
}
