package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_matkul.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://siasisten-plus.netlify.app/", "http://localhost:3001/"})
@RestController
@RequestMapping("/api/asdos-matkul")
public class AsdosMatkulApiController {

    @Autowired
    private MataKuliahService mataKuliahService;

    @GetMapping(path = "/{email}", produces = {"application/json"})
    public ResponseEntity<List<AsistenDosen>> getMataKuliahByEmailAsdos(@PathVariable(value = "email") String email) {
        return ResponseEntity.ok(mataKuliahService.getMataKuliahByEmailAsdos(email));
    }
}
