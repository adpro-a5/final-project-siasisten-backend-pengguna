package id.ac.cs.advpro.a05.finalprojectsiasisten.model.pendaftaran;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Pendaftaran")
@NoArgsConstructor
public class Pendaftaran {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "npm_mahasiswa")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Mahasiswa mahasiswa;

    @ManyToOne
    @JoinColumn(name = "mata_kuliah")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private MataKuliah mataKuliah;

    @Column
    private int sks;

    @Column
    private double ipk;

    @Column
    @Enumerated(EnumType.STRING)
    private StatusPendaftaran statusPendaftaran = StatusPendaftaran.MENDAFTAR;

    public Pendaftaran(Mahasiswa mahasiswa, MataKuliah mataKuliah) {
        this.mahasiswa = mahasiswa;
        this.mataKuliah = mataKuliah;
    }
}
