package id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna;

import id.ac.cs.advpro.a05.finalprojectsiasisten.core.Authority;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AkunRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private AkunRepository akunRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<Mahasiswa> fetchAllMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByEmail(String email) {
        return mahasiswaRepository.findByAkunEmail(email);
    }

    @Override
    public Mahasiswa addMahasiswa(Map<String, String> akunMap, Map<String, String> mahasiswaMap) {
        Akun akun = new Akun(akunMap.get("email"), akunMap.get("password"), akunMap.get("nomorTelepon"), Roles.MAHASISWA);
        akun.setPassword(passwordEncoder.encode(akun.getPassword()));
        akun.setAuthority(Authority.getInstance(akun.getRole().name()).getAuthority());
        akunRepository.save(akun);
        return mahasiswaRepository.save(new Mahasiswa(akun, mahasiswaMap.get("npmMahasiswa"), mahasiswaMap.get("namaMahasiswa")));
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Map<String, String> updated) {
        Mahasiswa updatedMahasiswa = mahasiswaRepository.findMahasiswaByNpmMahasiswa(npm);
        Akun updatedAkun = updatedMahasiswa.getAkun();

        updatedAkun.setNomorTelepon(updated.get("nomorTelepon"));
        updatedMahasiswa.setAkun(updatedAkun);
        updatedMahasiswa.setWaktuKosong(updated.get("waktuKosong"));
        updatedMahasiswa.setNamaBank(updated.get("namaBank"));
        updatedMahasiswa.setNomorRekening(updated.get("nomorRekening"));

        akunRepository.save(updatedAkun);
        mahasiswaRepository.save(updatedMahasiswa);

        return updatedMahasiswa;
    }

}
