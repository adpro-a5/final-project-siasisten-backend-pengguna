package id.ac.cs.advpro.a05.finalprojectsiasisten.repository;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.DosenMK;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DosenMKRepository extends JpaRepository<DosenMK, String> {
    DosenMK findDosenMKByDosenAndMataKuliah(Dosen dosen, MataKuliah mataKuliah);
    List<DosenMK> findAllByDosenAkunEmail(String email);
}