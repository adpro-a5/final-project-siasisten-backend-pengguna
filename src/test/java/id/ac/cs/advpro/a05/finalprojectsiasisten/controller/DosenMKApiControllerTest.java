package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.controller.DosenMKApiController;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.DosenMK;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_matkul.MataKuliahService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(controllers = DosenMKApiController.class)
class DosenMKApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MataKuliahService mataKuliahService;

    private final List<DosenMK> dosenMKList = new ArrayList<>();
    private MataKuliah mataKuliah1;

    @BeforeEach
    void setUp() {
        Akun akun = new Akun("a", "b", "c", Roles.DOSEN);
        Dosen dosen = new Dosen(akun, "a", "b");
        mataKuliah1 = new MataKuliah("a", "b", 1);
        MataKuliah mataKuliah2 = new MataKuliah("c", "d", 1);
        DosenMK dosenMK1 = new DosenMK(dosen, mataKuliah1);
        DosenMK dosenMK2 = new DosenMK(dosen, mataKuliah2);
        dosenMKList.add(dosenMK1);
        dosenMKList.add(dosenMK2);
    }

    @Test
    void testGetListDosenMK() throws Exception {
        when(mataKuliahService.getMataKuliahByEmailDosen("a")).thenReturn(dosenMKList);

        mockMvc.perform(get("/api/dosen-matkul/a").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(0))
                .andExpect(jsonPath("$[0].mataKuliah").value(mataKuliah1));
    }
}
