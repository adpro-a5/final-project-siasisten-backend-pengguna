package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna.DosenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = DosenApiController.class)
class DosenApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DosenService dosenService;

    private final Map<String, Map<String, String>> map = new HashMap<>();
    private final Map<String, String> akunMap = new HashMap<>();
    private final Map<String, String> dosenMap = new HashMap<>();
    private final Map<String, Boolean> mapUpdate = new HashMap<>();
    private Dosen dosen;

    @BeforeEach
    void setUp() {
        akunMap.put("email", "ramdhan@gmail.com");
        akunMap.put("password", "abcde12345");
        akunMap.put("nomorTelepon", "085224422524");
        dosenMap.put("nidnDosen", "2410");
        dosenMap.put("namaDosen", "ramdhan");
        map.put("akun", akunMap);
        map.put("dosen", dosenMap);
        Akun akun = new Akun(akunMap.get("email"), akunMap.get("password"), akunMap.get("nomorTelepon"), Roles.DOSEN);
        dosen = new Dosen();
        dosen.setAkun(akun);
        dosen.setNidnDosen(dosenMap.get("nidnDosen"));
        dosen.setNamaDosen(dosenMap.get("namaDosen"));

        mapUpdate.put("action", true);
    }

    @Test
    void testPostDosen() throws Exception {
        when(dosenService.addDosen(akunMap, dosenMap)).thenReturn(dosen);

        mockMvc.perform(post("/api/dosen")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(map)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nidnDosen").value(dosenMap.get("nidnDosen")))
                .andExpect(jsonPath("$.namaDosen").value(dosenMap.get("namaDosen")));
    }

    @Test
    void testGetNonExisastentDosenByEmail() throws Exception {
        mockMvc.perform(get("/api/dosen/ramdhan@gmail.com").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testGetDosenByEmail() throws Exception {
        when(dosenService.getDosenByEmail("ramdhan@gmail.com")).thenReturn(dosen);

        mockMvc.perform(get("/api/dosen/ramdhan@gmail.com").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nidnDosen").value(dosenMap.get("nidnDosen")))
                .andExpect(jsonPath("$.namaDosen").value(dosenMap.get("namaDosen")));
    }

    @Test
    void testGetListDosen() throws Exception {
        List<Dosen> dosenList = List.of(dosen);
        when(dosenService.fetchAllDosen()).thenReturn(dosenList);

        mockMvc.perform(get("/api/dosen").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].nidnDosen").value(dosenMap.get("nidnDosen")))
                .andExpect(jsonPath("$[0].namaDosen").value(dosenMap.get("namaDosen")));
    }

    @Test
    void testUpdateDosen() throws Exception {
        dosen.setIsApprove(true);
        when(dosenService.updateDosen("2410", mapUpdate.get("action"))).thenReturn(dosen);

        mockMvc.perform(put("/api/dosen/2410")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(mapUpdate)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.isApprove").value(true));
    }

}
