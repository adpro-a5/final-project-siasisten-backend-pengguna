package id.ac.cs.advpro.a05.finalprojectsiasisten.core;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AuthorityTest {

    @Test
    void testAuthority()
    {
        Authority singleton = Authority.getInstance(Roles.DOSEN.name());
        SimpleGrantedAuthority simpleGrantedAuthority = singleton.getAuthority();

        Authority singleton2 = Authority.getInstance(Roles.KOORDINATOR.name());
        SimpleGrantedAuthority simpleGrantedAuthority2 = singleton2.getAuthority();

        assertEquals(singleton, singleton2);
        assertEquals(simpleGrantedAuthority, simpleGrantedAuthority2);
    }

    @Test
    void testGetName() {
        Authority singleton4 = Authority.getInstance(Roles.ADMIN.name());
        assertEquals(singleton4.getName(), Roles.ADMIN.name());
    }

    @Test
    void testSetName() {
        Authority singleton5 = Authority.getInstance(Roles.ADMIN.name());
        singleton5.setName(Roles.DOSEN.name());
        assertEquals(singleton5.getName(), Roles.DOSEN.name());
    }
}
