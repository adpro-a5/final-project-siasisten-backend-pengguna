package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna.MahasiswaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MahasiswaApiController.class)
class MahasiswaApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MahasiswaService mahasiswaService;

    private final Map<String, Map<String, String>> map = new HashMap<>();
    private final Map<String, String> akunMap = new HashMap<>();
    private final Map<String, String> mahasiswaMap = new HashMap<>();
    private Akun akun;
    private Mahasiswa mahasiswa;

    @BeforeEach
    void setUp() {
        akunMap.put("email", "ramdhan@gmail.com");
        akunMap.put("password", "abcde12345");
        akunMap.put("nomorTelepon", "085224422524");
        mahasiswaMap.put("npmMahasiswa", "2410");
        mahasiswaMap.put("namaMahasiswa", "ramdhan");
        map.put("akun", akunMap);
        map.put("mahasiswa", mahasiswaMap);
        akun = new Akun(akunMap.get("email"), akunMap.get("password"), akunMap.get("nomorTelepon"), Roles.MAHASISWA);
        mahasiswa = new Mahasiswa(akun, mahasiswaMap.get("npmMahasiswa"), mahasiswaMap.get("namaMahasiswa"));
    }

    @Test
    void testPostMahasiswa() throws Exception {
        when(mahasiswaService.addMahasiswa(akunMap, mahasiswaMap)).thenReturn(mahasiswa);

        mockMvc.perform(post("/api/mahasiswa")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(map)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.npmMahasiswa").value(mahasiswaMap.get("npmMahasiswa")))
                .andExpect(jsonPath("$.namaMahasiswa").value(mahasiswaMap.get("namaMahasiswa")));
    }

    @Test
    void testGetListMahasiswa() throws Exception {
        List<Mahasiswa> mahasiswaList = List.of(mahasiswa);
        when(mahasiswaService.fetchAllMahasiswa()).thenReturn(mahasiswaList);

        mockMvc.perform(get("/api/mahasiswa").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].npmMahasiswa").value(mahasiswaMap.get("npmMahasiswa")))
                .andExpect(jsonPath("$[0].namaMahasiswa").value(mahasiswaMap.get("namaMahasiswa")));
    }

    @Test
    void testGetNonExistentMahasiswaByNPM() throws Exception {
        mockMvc.perform(get("/api/mahasiswa/ramdhan@gmail.com").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testGetMahasiswaByEmail() throws Exception {
        when(mahasiswaService.getMahasiswaByEmail("ramdhan@gmail.com")).thenReturn(mahasiswa);

        mockMvc.perform(get("/api/mahasiswa/ramdhan@gmail.com").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.npmMahasiswa").value(mahasiswaMap.get("npmMahasiswa")))
                .andExpect(jsonPath("$.namaMahasiswa").value(mahasiswaMap.get("namaMahasiswa")));
    }

    @Test
    void testUpdateMahasiswa() throws Exception {
        Map<String, String> updatedMahasiswa = new HashMap<>();
        updatedMahasiswa.put("nomorTelepon", "085");
        updatedMahasiswa.put("waktuKosong", "bebas");
        updatedMahasiswa.put("namaBank", "bjb");
        updatedMahasiswa.put("nomorRekening", "2424");
        mahasiswaService.addMahasiswa(akunMap, mahasiswaMap);
        mahasiswa.setWaktuKosong("malam");
        when(mahasiswaService.updateMahasiswa("2410", updatedMahasiswa)).thenReturn(mahasiswa);

        mockMvc.perform(put("/api/mahasiswa/2410")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(updatedMahasiswa)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.waktuKosong").value("malam"));
    }

}
