package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestAssignKoordinator;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos.KoordinatorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = KoordinatorApiController.class)
class KoordinatorApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private KoordinatorService koordinatorService;

    @Test
    void testAssignKoordinator() throws Exception {
        RequestAssignKoordinator assignKoordinator = new RequestAssignKoordinator();
        assignKoordinator.setIdMataKuliah("0");
        assignKoordinator.setNpmMahasiswa("000");

        mockMvc.perform(post("/api/koordinator")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(assignKoordinator)))
                .andExpect(status().isOk());
    }

}
