package id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna;

import id.ac.cs.advpro.a05.finalprojectsiasisten.core.Authority;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AkunRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MahasiswaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
 class MahasiswaServiceTest {
    private Class<?> mahasiswaServiceClass;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private AkunRepository akunRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    private Mahasiswa mahasiswa;
    private Akun akun;

    @BeforeEach
    void setup() throws Exception {
        mahasiswaServiceClass = Class.forName("id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna.MahasiswaService");
        akun = new Akun("akun@mail.com", "a", "888", Roles.MAHASISWA);
        mahasiswa = new Mahasiswa(akun, "000", "a");
    }

    @Test
    void testMahasiswaServiceClassHasFetchAllMahasiswaMethod() throws Exception {
        Method fetchAllMahasiswa = mahasiswaServiceClass.getDeclaredMethod("fetchAllMahasiswa");
        int methodModifiers = fetchAllMahasiswa.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, fetchAllMahasiswa.getParameterCount());
    }

    @Test
    void testFetchAllMahasiswa() {
        assertEquals(mahasiswaService.fetchAllMahasiswa(), mahasiswaRepository.findAll());
        assertEquals(mahasiswaService.fetchAllMahasiswa().getClass(), mahasiswaRepository.findAll().getClass());
    }

    @Test
    void testMahasiswaServiceClassHasAddMahasiswaMethod() throws Exception {
        Method addMahasiswa = mahasiswaServiceClass.getDeclaredMethod("addMahasiswa",
                Map.class, Map.class);
        int methodModifiers = addMahasiswa.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, addMahasiswa.getParameterCount());
    }

    @Test
    void testAddMahasiswa() {
        Map<String, String> akunMap = new HashMap<>();
        akunMap.put("email", "akun@mail.com");
        akunMap.put("password", "a");
        akunMap.put("nomorTelepon", "888");

        Map<String, String> mahasiswaMap = new HashMap<>();
        mahasiswaMap.put("npmMahasiswa", "000");
        mahasiswaMap.put("namaMahasiswa", "a");
        akun.setAuthority(Authority.getInstance(akun.getRole().name()).getAuthority());

        when(passwordEncoder.encode("a")).thenReturn("a");
        mahasiswaService.addMahasiswa(akunMap, mahasiswaMap);
        verify(akunRepository, times(1)).save(akun);
    }

    @Test
    void testMahasiswaServiceClassHasGetMahasiswaByEmailMethod() throws Exception {
        Method getMahasiswaByEmail = mahasiswaServiceClass.getDeclaredMethod("getMahasiswaByEmail",
                String.class);
        int methodModifiers = getMahasiswaByEmail.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getMahasiswaByEmail.getParameterCount());
    }

    @Test
    void testGetMahasiswaByEmail() {
        when(mahasiswaRepository.findByAkunEmail(akun.getEmail())).thenReturn(mahasiswa);
        assertEquals(mahasiswaService.getMahasiswaByEmail(akun.getEmail()), mahasiswa);
    }

    @Test
    void testUpdateMahasiswa() {
        Map<String, String> updatedMahasiswa = new HashMap<>();
        updatedMahasiswa.put("nomorTelepon", "085");
        updatedMahasiswa.put("waktuKosong", "bebas");
        updatedMahasiswa.put("namaBank", "bjb");
        updatedMahasiswa.put("nomorRekening", "2424");
        when(mahasiswaRepository.findMahasiswaByNpmMahasiswa("000")).thenReturn(mahasiswa);
        mahasiswaService.updateMahasiswa("000", updatedMahasiswa);
        assertEquals(updatedMahasiswa.get("nomorTelepon"), mahasiswa.getAkun().getNomorTelepon());
        assertEquals(updatedMahasiswa.get("waktuKosong"), mahasiswa.getWaktuKosong());
        assertEquals(updatedMahasiswa.get("namaBank"), mahasiswa.getNamaBank());
        assertEquals(updatedMahasiswa.get("nomorRekening"), mahasiswa.getNomorRekening());
        verify(mahasiswaRepository, times(1)).save(mahasiswa);
    }

}