package id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.pendaftaran.Pendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestActionPendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreatePendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AsdosRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MahasiswaRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MataKuliahRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.PendaftaranRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PendaftaranServiceTest {
    private Class<?> pendaftaranServiceClass;

    @Mock
    private PendaftaranRepository pendaftaranRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private AsdosRepository asdosRepository;

    @InjectMocks
    private PendaftaranServiceImpl pendaftaranService;

    private Pendaftaran pendaftaran;
    private MataKuliah mataKuliah;
    private Mahasiswa mahasiswa;
    private RequestCreatePendaftaran mapPendaftaran;

    @BeforeEach
    void setup() throws Exception {
        pendaftaranServiceClass = Class.forName("id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos.PendaftaranService");

        Akun akun = new Akun("akun@mail.com", "a", "888", Roles.MAHASISWA);
        mahasiswa = new Mahasiswa(akun, "000", "a");
        mataKuliah = new MataKuliah("0", "b", 1);
        pendaftaran = new Pendaftaran(mahasiswa, mataKuliah);
        pendaftaran.setId(0);
        pendaftaran.setIpk(3.0);
        pendaftaran.setSks(22);

        mapPendaftaran = new RequestCreatePendaftaran();
        mapPendaftaran.setIdMataKuliah("0");
        mapPendaftaran.setNpmMahasiswa("000");
        mapPendaftaran.setSks(22);
        mapPendaftaran.setIpk(3.0);
    }

    @Test
    void testPendaftaranServiceHasGetPendaftaranMethod() throws Exception {
        Method addPendaftaran = pendaftaranServiceClass.getDeclaredMethod("getPendaftaran",
                String .class, String.class);
        int methodModifiers = addPendaftaran.getModifiers();

        Assertions.assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, addPendaftaran.getParameterCount());
    }

    @Test
    void testGetPendaftaran() {
        when(pendaftaranRepository.findPendaftaranByMahasiswaNpmMahasiswaAndMataKuliahIdMataKuliah("000","0" ))
                .thenReturn(pendaftaran);
        assertEquals(pendaftaranService.getPendaftaran("0", "000"), pendaftaran);
    }

    @Test
    void testPendaftaranServiceHasAddPendaftaranMethod() throws Exception {
        Method addPendaftaran = pendaftaranServiceClass.getDeclaredMethod("addPendaftaran",
                RequestCreatePendaftaran.class);
        int methodModifiers = addPendaftaran.getModifiers();

        Assertions.assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, addPendaftaran.getParameterCount());
    }

    @Test
    void testAddPendaftaran() {
        lenient().when(mataKuliahRepository.findMataKuliahByIdMataKuliah("0")).thenReturn(mataKuliah);
        lenient().when(mahasiswaRepository.findMahasiswaByNpmMahasiswa("000")).thenReturn(mahasiswa);
        assertEquals(pendaftaranService.addPendaftaran(mapPendaftaran), pendaftaran);
    }

    @Test
    void testPendaftaranServiceHasGetPendaftaranByMataKuliahMethod() throws Exception {
        Method getPendaftaranByMataKuliah = pendaftaranServiceClass.getDeclaredMethod("getPendaftaranByMataKuliah",
                String.class);
        int methodModifiers = getPendaftaranByMataKuliah.getModifiers();

        Assertions.assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getPendaftaranByMataKuliah.getParameterCount());
    }

    @Test
    void testGetPendaftaranByMataKuliah() {
        List<Pendaftaran> listPendaftaran = pendaftaranRepository.findAllByMataKuliah(mataKuliah);
        lenient().when(pendaftaranService.getPendaftaranByMataKuliah("0")).thenReturn(listPendaftaran);
        List<Pendaftaran> listPendaftaranResult = pendaftaranService.getPendaftaranByMataKuliah("0");
        assertIterableEquals(listPendaftaran, listPendaftaranResult);
    }

    @Test
    void testPendaftaranServiceHasActionAsdosMethod() throws Exception {
        Method actionAsdos = pendaftaranServiceClass.getDeclaredMethod("actionAsdos",
                String.class, RequestActionPendaftaran.class);
        int methodModifiers = actionAsdos.getModifiers();

        Assertions.assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, actionAsdos.getParameterCount());
    }

    @Test
    void testActionAsdosDiterima() {
        RequestActionPendaftaran actionPendaftaran = new RequestActionPendaftaran();
        actionPendaftaran.setNpmMahasiswa("2410");
        actionPendaftaran.setAction("DITERIMA");

        lenient().when(mataKuliahRepository.findMataKuliahByIdMataKuliah("0")).thenReturn(mataKuliah);
        lenient().when(mahasiswaRepository.findMahasiswaByNpmMahasiswa("2410")).thenReturn(mahasiswa);
        lenient().when(pendaftaranRepository.findPendaftaranByMahasiswaAndMataKuliah(mahasiswa, mataKuliah)).thenReturn(pendaftaran);

        pendaftaranService.actionAsdos("0", actionPendaftaran);
        verify(pendaftaranRepository, times(1)).save(pendaftaran);
    }

    @Test
    void testActionAsdosOther() {
        RequestActionPendaftaran actionPendaftaran = new RequestActionPendaftaran();
        actionPendaftaran.setNpmMahasiswa("2410");
        actionPendaftaran.setAction("DITOLAK");

        lenient().when(mataKuliahRepository.findMataKuliahByIdMataKuliah("0")).thenReturn(mataKuliah);
        lenient().when(mahasiswaRepository.findMahasiswaByNpmMahasiswa("2410")).thenReturn(mahasiswa);
        lenient().when(pendaftaranRepository.findPendaftaranByMahasiswaAndMataKuliah(mahasiswa, mataKuliah)).thenReturn(pendaftaran);

        pendaftaranService.actionAsdos("0", actionPendaftaran);
        verify(pendaftaranRepository, times(1)).save(pendaftaran);
    }

}
