package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.core.Authority;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestLogin;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna.AkunDetailService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = LoginApiController.class)
class LoginApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @MockBean
    private AkunDetailService akunDetailService;

    private Akun akun;
    private RequestLogin requestLogin;

    @BeforeEach
    void setUp() {
        akun = new Akun("a", "a", "a", Roles.ADMIN);
        akun.setPassword(passwordEncoder.encode(akun.getPassword()));
        akun.setAuthority(Authority.getInstance(String.valueOf(Roles.ADMIN)).getAuthority());

        requestLogin = new RequestLogin();
        requestLogin.setEmail("a");
        requestLogin.setPassword("a");
    }

    @Test
    void testUserLoginAnonymousUser() throws Exception {
        mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(requestLogin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.principal").value("anonymousUser"))
                .andExpect(jsonPath("$.name").value("anonymousUser"))
                .andExpect(jsonPath("$.authenticated").value(true));
    }

    @Test
    void testUserLoginWrongPassword() throws Exception {
        when(akunDetailService.getAkunByEmail("a")).thenReturn(Optional.of(akun));
        requestLogin.setPassword("b");
        mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(requestLogin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.principal").value("anonymousUser"))
                .andExpect(jsonPath("$.name").value("anonymousUser"))
                .andExpect(jsonPath("$.authenticated").value(true));
    }

    @Test
    void testUserLogin() throws Exception {
        when(akunDetailService.getAkunByEmail("a")).thenReturn(Optional.of(akun));
        mockMvc.perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(requestLogin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.principal.email").value("a"))
                .andExpect(jsonPath("$.credentials").value("a"))
                .andExpect(jsonPath("$.authenticated").value(true))
                .andExpect(jsonPath("$.name").value("a"));
    }
}
