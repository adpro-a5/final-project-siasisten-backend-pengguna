package id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class KoordinatorTest {

    private Koordinator koordinator;
    private Koordinator koordinator2;

    @BeforeEach
    void setup() {
        koordinator = new Koordinator();
        koordinator2 = new Koordinator();
    }

    @Test
    void testKoordinator() {
        assertEquals(koordinator.getClass().getName(), koordinator2.getClass().getName());
    }
}
