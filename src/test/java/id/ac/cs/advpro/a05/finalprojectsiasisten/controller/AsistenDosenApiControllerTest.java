package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos.AsistenDosenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AsistenDosenApiController.class)
class AsistenDosenApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AsistenDosenService asistenDosenService;

    List<AsistenDosen> asistenDosenList = new ArrayList<>();
    MataKuliah mataKuliah;
    Mahasiswa mahasiswa1;

    @BeforeEach
    void setUp() {
        mataKuliah = new MataKuliah("0", "0", 1);
        mahasiswa1 = new Mahasiswa(new Akun(), "b", "c");
        AsistenDosen asistenDosen1 = new AsistenDosen(mahasiswa1, mataKuliah);
        asistenDosenList.add(asistenDosen1);
    }

    @Test
    void testGetAsistenDosenByIdMataKuliah() throws Exception {
        when(asistenDosenService.getAsistenDosenByIdMataKuliah("0")).thenReturn(asistenDosenList);

        mockMvc.perform(get("/api/asisten-dosen/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(0))
                .andExpect(jsonPath("$[0].mataKuliah").value(mataKuliah));
    }

}
