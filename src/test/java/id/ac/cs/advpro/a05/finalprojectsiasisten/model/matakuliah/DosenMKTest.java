package id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DosenMKTest {
    private Dosen dosen;
    private MataKuliah mataKuliah;
    private DosenMK dosenMK;

    @BeforeEach
    void setup() {
        dosen = new Dosen();
        mataKuliah = new MataKuliah();
        dosenMK = new DosenMK(dosen, mataKuliah);
    }

    @Test
    void testAsistenDosen() {
        assertEquals(dosenMK.getDosen(), dosen);
    }
}
