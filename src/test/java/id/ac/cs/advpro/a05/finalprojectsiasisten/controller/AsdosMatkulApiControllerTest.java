package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.controller.AsdosMatkulApiController;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_matkul.MataKuliahService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AsdosMatkulApiController.class)
class AsdosMatkulApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MataKuliahService mataKuliahService;

    private final List<AsistenDosen> asistenDosenList = new ArrayList<>();
    private MataKuliah mataKuliah1;

    @BeforeEach
    void setUp() {
        Akun akun = new Akun("a", "b", "c", Roles.DOSEN);
        Mahasiswa mahasiswa = new Mahasiswa(akun, "a", "b");
        mataKuliah1 = new MataKuliah("a", "b", 1);
        MataKuliah mataKuliah2 = new MataKuliah("c", "d", 1);
        AsistenDosen asistenDosen1 = new AsistenDosen(mahasiswa, mataKuliah1);
        AsistenDosen asistenDosen2 = new AsistenDosen(mahasiswa, mataKuliah2);
        asistenDosenList.add(asistenDosen1);
        asistenDosenList.add(asistenDosen2);
    }

    @Test
    void testGetListDosenMK() throws Exception {
        when(mataKuliahService.getMataKuliahByEmailAsdos("a")).thenReturn(asistenDosenList);

        mockMvc.perform(get("/api/asdos-matkul/a").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(0))
                .andExpect(jsonPath("$[0].mataKuliah").value(mataKuliah1));
    }
}
