package id.ac.cs.advpro.a05.finalprojectsiasisten.service_matkul;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreateMataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AsdosRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.DosenMKRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.DosenRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MataKuliahRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MataKuliahServiceImplTest {
    private Class<?> mataKuliahServiceClass;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @Mock
    private DosenMKRepository dosenMKRepository;

    @Mock
    private DosenRepository dosenRepository;

    @Mock
    private AsdosRepository asdosRepository;

    @InjectMocks
    private MataKuliahServiceImpl mataKuliahService;

    private MataKuliah mataKuliah;
    private Dosen dosenOne;
    private Dosen dosenTwo;

    @BeforeEach
    void setup() throws Exception {
        mataKuliahServiceClass = Class.forName("id.ac.cs.advpro.a05.finalprojectsiasisten.service_matkul.MataKuliahService");
        mataKuliah = new MataKuliah();
        mataKuliah.setIdMataKuliah("0");
        mataKuliah.setNamaMataKuliah("Kalkulus 2");
        mataKuliah.setJumlahLowongan(5);
        mataKuliah.setRegisteredAt(new Date());

        Akun akunOne = new Akun();
        Akun akunTwo = new Akun();
        dosenOne = new Dosen(akunOne, "1", "a");
        dosenTwo = new Dosen(akunTwo, "2", "b");
    }

    @Test
    void testMataKuliahServiceClassHasFetchAllMataKuliahMethod() throws Exception {
        Method fetchAllMataKuliah = mataKuliahServiceClass.getDeclaredMethod("fetchAllMataKuliah");
        int methodModifiers = fetchAllMataKuliah.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, fetchAllMataKuliah.getParameterCount());
    }

    @Test
    void testFetchAllMataKuliah() {
        assertEquals(mataKuliahService.fetchAllMataKuliah(), mataKuliahRepository.findAll());
        assertEquals(mataKuliahService.fetchAllMataKuliah().getClass(), mataKuliahRepository.findAll().getClass());
    }

    @Test
    void testMataKuliahServiceClassHasAddMataKuliahMethod() throws Exception {
        Method addMataKuliah = mataKuliahServiceClass.getDeclaredMethod("addMataKuliah",
                RequestCreateMataKuliah.class);
        int methodModifiers = addMataKuliah.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, addMataKuliah.getParameterCount());
    }

    @Test
    void testAddMataKuliahParamsMapBody() {
        RequestCreateMataKuliah map = new RequestCreateMataKuliah();
        List<String> listDosen = new ArrayList<>();
        listDosen.add("1");
        listDosen.add("2");
        map.setIdMataKuliah("0");
        map.setNamaMataKuliah("Kalkulus 2");
        map.setJumlahLowongan("8");
        map.setDosen(listDosen);

        when(dosenRepository.findDosenByNidnDosen("1")).thenReturn(dosenOne);
        when(dosenRepository.findDosenByNidnDosen("2")).thenReturn(dosenTwo);
        assertEquals(mataKuliahService.addMataKuliah(map).getClass(), mataKuliah.getClass());
    }

    @Test
    void testAddMataKuliahWithBlankId() {
        RequestCreateMataKuliah map = new RequestCreateMataKuliah();
        List<String> listDosen = new ArrayList<>();
        listDosen.add("1");
        listDosen.add("2");
        map.setIdMataKuliah("");
        map.setNamaMataKuliah("Kalkulus 2");
        map.setJumlahLowongan("8");
        map.setDosen(listDosen);
        assertEquals(mataKuliahService.addMataKuliah(map).getClass(), mataKuliah.getClass());
    }

    @Test
    void testAddMataKuliahWithBlankJumlahLowongan() {
        RequestCreateMataKuliah map = new RequestCreateMataKuliah();
        List<String> listDosen = new ArrayList<>();
        listDosen.add("1");
        listDosen.add("2");
        map.setIdMataKuliah("0");
        map.setNamaMataKuliah("Kalkulus 2");
        map.setJumlahLowongan("");
        map.setDosen(listDosen);

        assertEquals(mataKuliahService.addMataKuliah(map).getClass(), mataKuliah.getClass());
    }

    @Test
    void testMataKuliahServiceClassHasGetMataKuliahByEmailDosenMethod() throws Exception {
        Method getMataKuliahByEmailDosen = mataKuliahServiceClass.getDeclaredMethod("getMataKuliahByEmailDosen",
                String.class);
        int methodModifiers = getMataKuliahByEmailDosen.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getMataKuliahByEmailDosen.getParameterCount());
    }

    @Test
    void testGetMataKuliahByEmailDosen() {
        assertEquals(mataKuliahService.getMataKuliahByEmailDosen("email"), dosenMKRepository.findAllByDosenAkunEmail("email"));
        assertEquals(mataKuliahService.getMataKuliahByEmailDosen("email").getClass(), dosenMKRepository.findAllByDosenAkunEmail("email").getClass());
    }

    @Test
    void testMataKuliahServiceClassHasGetMataKuliahByEmailAsdosMethod() throws Exception {
        Method getMataKuliahByEmailAsdos = mataKuliahServiceClass.getDeclaredMethod("getMataKuliahByEmailAsdos",
                String.class);
        int methodModifiers = getMataKuliahByEmailAsdos.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getMataKuliahByEmailAsdos.getParameterCount());
    }

    @Test
    void testGetMataKuliahByEmailAsdos() {
        assertEquals(mataKuliahService.getMataKuliahByEmailAsdos("email"), asdosRepository.findAllByMahasiswaAkunEmail("email"));
        assertEquals(mataKuliahService.getMataKuliahByEmailAsdos("email").getClass(), asdosRepository.findAllByMahasiswaAkunEmail("email").getClass());
    }
}
