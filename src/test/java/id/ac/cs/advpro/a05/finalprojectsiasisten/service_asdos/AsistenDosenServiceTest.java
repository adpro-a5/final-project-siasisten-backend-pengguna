package id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AsdosRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class AsistenDosenServiceTest {
    private Class<?> asistenDosenServiceClass;

    @Mock
    private AsdosRepository asdosRepository;

    @InjectMocks
    private AsistenDosenServiceImpl asistenDosenService;

    @BeforeEach
    void setup() throws Exception {
        asistenDosenServiceClass = Class.forName("id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos.AsistenDosenService");
    }

    @Test
    void testAsistenDosenServiceHasGetAsistenDosenByIdMataKuliahMethod() throws Exception {
        Method getAsistenDosenByIdMataKuliah = asistenDosenServiceClass.getDeclaredMethod("getAsistenDosenByIdMataKuliah",
                String.class);
        int methodModifiers = getAsistenDosenByIdMataKuliah.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getAsistenDosenByIdMataKuliah.getParameterCount());
    }

    @Test
    void testGetAsistenDosenByIdMataKuliah() {
        List<AsistenDosen> asistenDosenList = asdosRepository.findAllByMataKuliahIdMataKuliah("0");
        lenient().when(asistenDosenService.getAsistenDosenByIdMataKuliah("0")).thenReturn(asistenDosenList);
        List<AsistenDosen> listPendaftaranResult = asistenDosenService.getAsistenDosenByIdMataKuliah("0");
        assertIterableEquals(asistenDosenList, listPendaftaranResult);
    }

}