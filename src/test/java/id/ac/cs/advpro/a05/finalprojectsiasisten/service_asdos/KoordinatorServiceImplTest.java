package id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.AsistenDosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Koordinator;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AsdosRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.KoordinatorRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MahasiswaRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@ExtendWith(MockitoExtension.class)
class KoordinatorServiceImplTest {
    private Class<?> koordinatorServiceClass;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private KoordinatorRepository koordinatorRepository;

    @Mock
    private AsdosRepository asdosRepository;

    @InjectMocks
    private KoordinatorServiceImpl koordinatorService;

    private MataKuliah mataKuliah;
    private Mahasiswa mahasiswa;
    private AsistenDosen asistenDosen;

    @BeforeEach
    void setup() throws Exception {
        koordinatorServiceClass = Class.forName("id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos.KoordinatorService");
        mahasiswa = new Mahasiswa(new Akun(), "0", "a");
        mataKuliah = new MataKuliah("000", "b", 1);
        asistenDosen = new AsistenDosen(mahasiswa, mataKuliah);
    }

    @Test
    void testKoordinatorServiceHasAssignAsdosMethod() throws Exception {
        Method assignKoor = koordinatorServiceClass.getDeclaredMethod("assignKoor",
                String.class, String.class);
        int methodModifiers = assignKoor.getModifiers();

        Assertions.assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, assignKoor.getParameterCount());
    }

    @Test
    void testAssignAsdos() {
        when(mahasiswaRepository.findMahasiswaByNpmMahasiswa("0")).thenReturn(mahasiswa);
        when(mataKuliahRepository.findMataKuliahByIdMataKuliah("000")).thenReturn(mataKuliah);
        when(asdosRepository.findAsistenDosenByMahasiswaAndMataKuliah(mahasiswa, mataKuliah)).thenReturn(asistenDosen);
        when(koordinatorRepository.findKoordinatorByMahasiswaAndMataKuliah(mahasiswa, mataKuliah)).thenReturn(null);
        koordinatorService.assignKoor("0", "000");

        assertTrue("", asistenDosen.getIsKoordinator());
    }

    @Test
    void testAssignAsdosNotNull() {
        when(mahasiswaRepository.findMahasiswaByNpmMahasiswa("0")).thenReturn(mahasiswa);
        when(mataKuliahRepository.findMataKuliahByIdMataKuliah("000")).thenReturn(mataKuliah);
        when(asdosRepository.findAsistenDosenByMahasiswaAndMataKuliah(mahasiswa, mataKuliah)).thenReturn(asistenDosen);
        when(koordinatorRepository.findKoordinatorByMahasiswaAndMataKuliah(mahasiswa, mataKuliah)).thenReturn(new Koordinator());
        koordinatorService.assignKoor("0", "000");

        assertFalse(asistenDosen.getIsKoordinator());
    }

}