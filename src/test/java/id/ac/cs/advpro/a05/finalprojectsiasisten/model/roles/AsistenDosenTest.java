package id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AsistenDosenTest {

    private AsistenDosen asistenDosen;
    private AsistenDosen asistenDosen2;

    @BeforeEach
    void setup() {
        asistenDosen = new AsistenDosen();
        asistenDosen2 = new AsistenDosen();
    }

    @Test
    void testAsistenDosen() {
        assertEquals(asistenDosen.getClass().getName(), asistenDosen2.getClass().getName());
    }
}
