package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.pendaftaran.Pendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.pendaftaran.StatusPendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Mahasiswa;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestActionPendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreatePendaftaran;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_asdos.PendaftaranService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PendaftaranApiController.class)
class PendaftaranApiControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PendaftaranService pendaftaranService;
    private MataKuliah mataKuliah;
    private Pendaftaran pendaftaran;
    private List<Pendaftaran> pendaftaranList;
    private RequestCreatePendaftaran mapPendaftaran;

    @BeforeEach
    void setUp() {
        mataKuliah = new MataKuliah("zr24", "adpro", 12);
        Akun akun = new Akun("ramdhan@gmail.com", "abcde12345", "085224422524", Roles.MAHASISWA);
        Mahasiswa mahasiswa = new Mahasiswa(akun, "2410", "ramdhan");
        pendaftaran = new Pendaftaran(mahasiswa, mataKuliah);
        pendaftaran.setId(0);

        pendaftaranList = new ArrayList<>();
        pendaftaranList.add(pendaftaran);

        mapPendaftaran = new RequestCreatePendaftaran();
        mapPendaftaran.setIdMataKuliah("zr24");
        mapPendaftaran.setNpmMahasiswa("2410");
        mapPendaftaran.setSks(18);
        mapPendaftaran.setIpk(3.4);

    }

    @Test
    void testPostPendaftaran() throws Exception {
        when(pendaftaranService.addPendaftaran(mapPendaftaran)).thenReturn(pendaftaran);

        mockMvc.perform(post("/api/pendaftaran")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(mapPendaftaran)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(0))
                .andExpect(jsonPath("$.mataKuliah").value(mataKuliah));
    }

    @Test
    void testPostPendaftaranNotNull() throws Exception {
        when(pendaftaranService.getPendaftaran("zr24", "2410")).thenReturn(new Pendaftaran());

        mockMvc.perform(post("/api/pendaftaran")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(mapPendaftaran)))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    void testGetPendaftaranById() throws Exception {
        when(pendaftaranService.getPendaftaranByMataKuliah("zr24")).thenReturn(pendaftaranList);

        mockMvc.perform(get("/api/pendaftaran/zr24").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(0))
                .andExpect(jsonPath("$[0].mataKuliah").value(mataKuliah));
    }

    @Test
    void testActionAsdos() throws Exception {
        RequestActionPendaftaran actionPendaftaran = new RequestActionPendaftaran();
        actionPendaftaran.setNpmMahasiswa("2410");
        actionPendaftaran.setAction("DITERIMA");
        pendaftaran.setStatusPendaftaran(StatusPendaftaran.DITERIMA);

        when(pendaftaranService.actionAsdos("zr24", actionPendaftaran)).thenReturn(pendaftaran);
        mockMvc.perform(put("/api/pendaftaran/zr24")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(actionPendaftaran)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

}
