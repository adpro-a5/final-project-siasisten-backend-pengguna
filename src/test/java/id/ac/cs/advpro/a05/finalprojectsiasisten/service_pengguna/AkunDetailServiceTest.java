package id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.*;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AkunRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AkunDetailServiceTest {
    Class<?> akunDetailServiceClass;

    @Mock
    private AkunRepository akunRepository;

    @InjectMocks
    AkunDetailsServiceImpl akunDetailService = new AkunDetailsServiceImpl();

    private Akun akun;
    private Admin admin;

    @BeforeEach
    void setup() throws Exception {
        akunDetailServiceClass = Class.forName("id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna.AkunDetailService");
        akun = new Akun("a", "b", "c", Roles.ADMIN);
        admin = new Admin(akun, "101");
    }

    @Test
    void whenLoadUserByUsernameShouldReturnAkun() {
        when(akunRepository.findAkunByEmail("a")).thenReturn(Optional.of(akun));
        assertEquals(akunDetailService.loadUserByUsername("a"), Optional.of(akun).get());
        assertEquals(admin.getAkun(), akun);
    }

    @Test
    void testAkunDetailsServiceHasGetDosenByEmailMethod() throws Exception {
        Method getAkunByEmail = akunDetailServiceClass.getDeclaredMethod("getAkunByEmail",
                String.class);
        int methodModifiers = getAkunByEmail.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getAkunByEmail.getParameterCount());
    }

    @Test
    void testGetDosenByEmail() {
        lenient().when(akunRepository.findAkunByEmail("dosen@mail.com")).thenReturn(Optional.ofNullable(akun));
        Optional<Akun> chosenAkun = akunDetailService.getAkunByEmail("dosen@mail.com");
        assertEquals(chosenAkun, akunRepository.findAkunByEmail("dosen@mail.com"));
    }
}
