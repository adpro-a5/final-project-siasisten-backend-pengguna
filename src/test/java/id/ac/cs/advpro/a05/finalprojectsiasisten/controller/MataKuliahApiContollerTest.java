package id.ac.cs.advpro.a05.finalprojectsiasisten.controller;

import id.ac.cs.advpro.a05.finalprojectsiasisten.model.matakuliah.MataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.payload.RequestCreateMataKuliah;
import id.ac.cs.advpro.a05.finalprojectsiasisten.service_matkul.MataKuliahService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MataKuliahApiController.class)
class MataKuliahApiContollerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MataKuliahService mataKuliahService;

    private final RequestCreateMataKuliah mapBody = new RequestCreateMataKuliah();
    private final List<String> stringList = new ArrayList<>();
    private MataKuliah mataKuliah;

    @BeforeEach
    void setUp() {
        mapBody.setIdMataKuliah("zr24");
        mapBody.setNamaMataKuliah("adpro");
        mapBody.setJumlahLowongan("12");
        stringList.add("2410");
        mapBody.setDosen(stringList);
        mataKuliah = new MataKuliah("zr24", "adpro", 12);
        Akun akun = new Akun("ramdhan@gmail.com", "abcde12345", "085224422524", Roles.DOSEN);
        Dosen dosen = new Dosen(akun, "2410", "ramdhan");
    }

    @Test
    void testPostMataKuliah() throws Exception {
        when(mataKuliahService.addMataKuliah(mapBody)).thenReturn(mataKuliah);

        mockMvc.perform(post("/api/mata-kuliah")
                        .contentType(MediaType.APPLICATION_JSON).content(Mapper.mapToJson(mapBody)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idMataKuliah").value("zr24"))
                .andExpect(jsonPath("$.namaMataKuliah").value("adpro"))
                .andExpect(jsonPath("$.jumlahLowongan").value(12));
    }

    @Test
    void testGetListMataKuliah() throws Exception {
        List<MataKuliah> mataKuliahList = List.of(mataKuliah);
        when(mataKuliahService.fetchAllMataKuliah()).thenReturn(mataKuliahList);

        mockMvc.perform(get("/api/mata-kuliah").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idMataKuliah").value("zr24"))
                .andExpect(jsonPath("$[0].namaMataKuliah").value("adpro"))
                .andExpect(jsonPath("$[0].jumlahLowongan").value(12));
    }
}
