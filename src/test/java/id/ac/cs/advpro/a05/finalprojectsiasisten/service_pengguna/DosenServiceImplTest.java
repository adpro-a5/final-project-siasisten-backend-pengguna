package id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna;

import id.ac.cs.advpro.a05.finalprojectsiasisten.core.Authority;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Akun;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Dosen;
import id.ac.cs.advpro.a05.finalprojectsiasisten.model.roles.Roles;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.AkunRepository;
import id.ac.cs.advpro.a05.finalprojectsiasisten.repository.DosenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DosenServiceImplTest {
    private Class<?> dosenServiceClass;

    @Mock
    private DosenRepository dosenRepository;

    @Mock
    private AkunRepository akunRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private DosenServiceImpl dosenService;

    private Dosen dosen;
    private Akun akun;

    @BeforeEach
    void setup() throws Exception {
        dosenServiceClass = Class.forName("id.ac.cs.advpro.a05.finalprojectsiasisten.service_pengguna.DosenService");
        akun = new Akun("dosen@mail.com", "akudosen", "08127739281", Roles.DOSEN);
        dosen = new Dosen(akun, "1234", "Dosen");
    }

    @Test
    void testDosenServiceClassHasFetchAllDosenMethod() throws Exception {
        Method fetchAllDosen = dosenServiceClass.getDeclaredMethod("fetchAllDosen");
        int methodModifiers = fetchAllDosen.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, fetchAllDosen.getParameterCount());
    }

    @Test
    void testFetchAllDosen() {
        assertEquals(dosenService.fetchAllDosen(), dosenRepository.findAll());
        assertEquals(dosenService.fetchAllDosen().getClass(), dosenRepository.findAll().getClass());
    }

    @Test
    void testDosenServiceClassHasAddDosenMethod() throws Exception {
        Method addDosen = dosenServiceClass.getDeclaredMethod("addDosen", Map.class, Map.class);
        int methodModifiers = addDosen.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, addDosen.getParameterCount());
    }

    @Test
    void testAddDosen() {
        Map<String, String> akunMap = new HashMap<>();
        akunMap.put("email", "dosen@mail.com");
        akunMap.put("password", "akudosen");
        akunMap.put("nomorTelepon", "08127739281");

        Map<String, String> dosenMap = new HashMap<>();
        dosenMap.put("nidnDosen", "1234");
        dosenMap.put("namaDosen", "Dosen");
        akun.setAuthority(Authority.getInstance(akun.getRole().name()).getAuthority());

        when(passwordEncoder.encode("akudosen")).thenReturn("akudosen");
        dosenService.addDosen(akunMap, dosenMap);
        verify(akunRepository, times(1)).save(akun);
    }

    @Test
    void testDosenServiceClassHasGetDosenByEmailMethod() throws Exception {
        Method getDosenByEmail = dosenServiceClass.getDeclaredMethod("getDosenByEmail", String.class);
        int methodModifiers = getDosenByEmail.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getDosenByEmail.getParameterCount());
    }

    @Test
    void testGetDosenByEmail() {
        lenient().when(dosenService.getDosenByEmail("dosen@mail.com")).thenReturn(dosen);
        Dosen chosenDosen = dosenService.getDosenByEmail("dosen@mail.com");
        assertEquals(chosenDosen.getNidnDosen(), dosen.getNidnDosen());
    }

    @Test
    void testDosenServiceClassHasUpdateDosenMethod() throws Exception {
        Method updateDosen = dosenServiceClass.getDeclaredMethod("updateDosen",
                String.class, Boolean.class);
        int methodModifiers = updateDosen.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, updateDosen.getParameterCount());
    }

    @Test
    void testUpdateDosenByNidn() {
        lenient().when(dosenRepository.findDosenByNidnDosen("1234")).thenReturn(dosen);
        dosenService.updateDosen("1234", true);
        assertTrue(dosen.getIsApprove());
    }

}