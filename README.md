# Final Project siasisten+
[![pipeline status](https://gitlab.com/adpro-a5/final-project-siasisten-backend-pengguna/badges/main/pipeline.svg)](https://gitlab.com/adpro-a5/final-project-siasisten-backend-pengguna/-/commits/main)

### Repository:
Repository Frontend: https://gitlab.com/adpro-a5/final-project-siasisten-frontend

Repository Backend - Pertama: https://gitlab.com/adpro-a5/final-project-siasisten-backend-pengguna

Repository Backend - Kedua: https://gitlab.com/adpro-a5/final-project-siasisten-backend-penilaian-penugasan

Repository Backend - Ketiga: https://gitlab.com/adpro-a5/final-project-siasisten-backend-log-asisten

### Link Penting:
Link Heroku Frontend: https://siasisten-plus.netlify.app/

Link Heroku Backend-1: https://siasisten-plus.herokuapp.com/

Link Heroku Backend-2: https://siasisten-plus-v2.herokuapp.com/

Link Heroku Backend-3: https://siasisten-plus-v3.herokuapp.com/

Link Sheet PBI: https://ristek.link/PBI-Siasisten+

Link Data Dummy: https://ristek.link/Data-Dummy-Siasisten+
